# Poll Api Companion Client (php)

This is a companion package for Poll Management API. - https://poll.mediaprima.com.my

## The API

The API is a consolidate place to accept and process all votes and results, when necessary, it also provides data feed with GET request.

## The Companion Client(s)

The companion clients may come in different language, for different website front-end client environment. In this example, a php version is provided. This PHP companion clients script integrates seamlessly with Guzzle with OAuth2 `Client Credentials` already implemented out of the box.

## Pre-Requisite

First configure the `client_id` and `secret_id` into the .env file. This credentials can be obtained from the Poll API `client websites` section.

## Web Poll Usage

### Fetch Poll Info
- To fetch the poll information, do a POST request to `/web/info.php` together with the required parameters.

```php
$_REQUEST['uuid']
```

### Pass Vote Request
- To pass a vote request to the API, do a POST request to `/web/vote.php` together with the required parameters.

```php
$_REQUEST['uuid'],
$_REQUEST['identity-type'],
$_REQUEST['answers'],
```

## Battle Poll Usage

### Fetch Poll Info
- To fetch the poll information, do a POST request to `/battle/info.php` together with the required parameters.

```php
$_REQUEST['uuid']
```

### Pass Vote Request
- To pass a vote request to the API, do a POST request to `/battle/vote.php` together with the required parameters.

```php
$_REQUEST['uuid'],
$_REQUEST['identity-type'],
$_REQUEST['answers']
```
