<?php

require_once __DIR__.'../../vendor/autoload.php';

use MediaPrima\PollApiClient\OAuthClient;

echo (new OAuthClient)->battleVote(
    $_REQUEST['uuid'],
    $_REQUEST['identity-type'],
    $_SERVER['REMOTE_ADDR'],
    date("Y-m-d H:i:s"),
    $_REQUEST['answers'],
    ''
  );
