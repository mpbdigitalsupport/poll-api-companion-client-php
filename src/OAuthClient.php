<?php

namespace MediaPrima\PollApiClient;

use Dotenv\Dotenv;
use GuzzleHttp\Client;
use kamermans\OAuth2\GrantType\ClientCredentials;
use kamermans\OAuth2\OAuth2Middleware;
use GuzzleHttp\HandlerStack;
use kamermans\OAuth2\Persistence\FileTokenPersistence;

class OAuthClient
{
    protected $clientId;
    protected $clientSecret;
    protected $apiBaseUrl;

    /**
     * @var Client
     */
    protected $client;


    public function __construct()
    {
        date_default_timezone_set('Asia/Kuala_Lumpur');

        $dotenv = new Dotenv(__DIR__.'/..');
        $dotenv->load();

        $this->clientId = getenv('CLIENT_ID');
        $this->clientSecret = getenv('CLIENT_SECRET');
        $this->apiBaseUrl = getenv('POLL_API_BASE_URL');
    }


    public function saySomething()
    {
        return $this->apiBaseUrl;
    }


    /**
     * @param string $endPoint
     *
     * @return string
     */
    protected function makeUrl($endPoint)
    {
        return $this->apiBaseUrl.DIRECTORY_SEPARATOR.ltrim($endPoint, '/');
    }


    /**
     * @return \GuzzleHttp\Client
     */
    protected function client()
    {
        $reauth_client = new Client(
            ['base_uri' => $this->apiBaseUrl.'/oauth/token',]
        );

        $reauth_config = [
            "client_id" => $this->clientId,
            "client_secret" => $this->clientSecret,
            "scope" => '*',
        ];

        $grant_type = new ClientCredentials($reauth_client, $reauth_config);
        $oauth = new OAuth2Middleware($grant_type);

        $oauth->setTokenPersistence(new FileTokenPersistence(__DIR__."/../token"));

        $stack = HandlerStack::create();
        $stack->push($oauth);

        return new Client(
            [
                'handler' => $stack,
                'auth' => 'oauth',
                'headers' => ['Accept' => 'application/json'],
            ]
        );
    }


    /**
     * @param $endPoint
     * @param $content
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function postToClient($endPoint, $content = null)
    {
        return $this->client()
            ->post(
                $this->makeUrl($endPoint),
                ['form_params' => $content]
            );
    }


    protected function getFromClient($endPoint)
    {
        return $this->client()
            ->get(
                $this->makeUrl($endPoint)
            );
    }


    /**
     * @param string      $uuid
     * @param int         $identity_type
     * @param string      $identity
     * @param string      $voting_time
     * @param array       $answers
     * @param null|string $extra_info
     *
     * @return array
     */
    protected function makeContents(
        $uuid,
        $identity_type,
        $identity,
        $voting_time,
        array $answers,
        $extra_info = null
    ) {
        return [
            'uuid' => $uuid,
            'identity_type' => $identity_type,
            'identity' => $identity,
            'voting_time' => $voting_time,
            'answers' => $answers,
            'extra_info' => $extra_info,
        ];
    }


    /**
     * @param string      $uuid
     * @param int         $identity_type
     * @param string      $identity
     * @param string      $voting_time
     * @param array       $answers
     * @param null|string $extra_info
     *
     * @return \Psr\Http\Message\StreamInterface
     */
    public function webVote(
        $uuid,
        $identity_type,
        $identity,
        $voting_time,
        array $answers,
        $extra_info = null
    ) {
        $response = $this->postToClient(
            '/api/draft-votes/webs',
            $this->makeContents(
                $uuid,
                $identity_type,
                $identity,
                $voting_time,
                $answers,
                $extra_info
            )
        );

        return $response->getBody();
    }


    /**
     * @param string      $uuid
     * @param int         $identity_type
     * @param string      $identity
     * @param string      $voting_time
     * @param array       $answers
     * @param null|string $extra_info
     *
     * @return \Psr\Http\Message\StreamInterface
     */
    public function battleVote(
        $uuid,
        $identity_type,
        $identity,
        $voting_time,
        array $answers,
        $extra_info = null
    ) {
        $response = $this->postToClient(
            '/api/draft-votes/battles',
            $this->makeContents(
                $uuid,
                $identity_type,
                $identity,
                $voting_time,
                $answers,
                $extra_info
            )
        );

        return $response->getBody();
    }


    /**
     * @param string      $uuid
     *
     * @return \Psr\Http\Message\StreamInterface
     */
     public function webInfo(
       $uuid
     ) {
       $response = $this->getFromClient(
         '/api/polls/webs/'.$uuid
       );

       return $response->getBody();
     }

     /**
      * @param string      $uuid
      *
      * @return \Psr\Http\Message\StreamInterface
      */
      public function battleInfo(
        $uuid
      ) {
        $response = $this->getFromClient(
          '/api/polls/battles/'.$uuid
        );

        return $response->getBody();
      }


      /**
       * @param string      $uuid
       *
       * @return \Psr\Http\Message\StreamInterface
       */
       public function result(
         $uuid
       ) {
         $response = $this->getFromClient(
           'api/polls/'.$uuid.'/result'
         );

         return $response->getBody();
       }


}

new OAuthClient();
