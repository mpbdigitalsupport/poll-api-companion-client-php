<?php
require_once __DIR__.'../../vendor/autoload.php';

use MediaPrima\PollApiClient\OAuthClient;
use Dotenv\Dotenv;

$dotenv = new Dotenv(__DIR__.'/..');
$dotenv->load();
$pollUUID = getenv('WEB_POLL_UUID');

for ($i = 0; $i < 5000; $i++) {

    $poll = (new OAuthClient)->webInfo($pollUUID);

    $collections = json_decode($poll);
    $options = [];
    foreach ($collections->options as $key => $value) {
      array_push($options, $value->uuid);
    };

    $randIP = "".mt_rand(0,255).".".mt_rand(0,255).".".mt_rand(0,255).".".mt_rand(0,255);
    $randOpt = array_rand(array_flip($options));

    $res = (new OAuthClient)->webVote(
        $pollUUID,
        2,
        $randIP,
        date("Y-m-d H:i:s"),
        [$randOpt],
        ''
    );

    print($res . ' : ' . $randIP . ' --> ' . $randOpt . ' --- ' . $i . PHP_EOL);

}
