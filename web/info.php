<?php

require_once __DIR__.'../../vendor/autoload.php';

use MediaPrima\PollApiClient\OAuthClient;

header('Content-Type: application/json');

echo (new OAuthClient)->webInfo(
    $_REQUEST['uuid']
);
